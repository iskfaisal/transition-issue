package com.seeken.transitionissue;

import android.view.View;

public interface AdapterListener {
    void itemClicked(int pos, ModelClass object, View view, String transition);
}
