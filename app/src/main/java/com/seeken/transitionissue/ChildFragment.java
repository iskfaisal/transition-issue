package com.seeken.transitionissue;


import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ChildFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ImageView cover;
        TextView title;

        View view = inflater.inflate(R.layout.fragment_child, container, false);

        cover = view.findViewById(R.id.cover);
        title = view.findViewById(R.id.title);

        Bundle b = getArguments();
        if (b != null) {

            String imageURL = b.getString("IMAGE_URL");
            int position = b.getInt("POSITION");

            Picasso.get().load(imageURL).into(cover);
            title.setText("Title " + (position+1));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String transitionName = b.getString("TRANSITION_NAME");
                cover.setTransitionName(transitionName);
                Log.i("CHILD FRAGMENT-DEBUG", transitionName );
            }
        }

        return view;
    }

}
