package com.seeken.transitionissue.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.seeken.transitionissue.AdapterListener;
import com.seeken.transitionissue.ChildFragment;
import com.seeken.transitionissue.MainActivity;
import com.seeken.transitionissue.ModelClass;
import com.seeken.transitionissue.R;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements AdapterListener {

    RecyclerView recyclerView;
    private ArrayList<String> images = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = root.findViewById(R.id.recycler_view);
        getImages();
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(manager);
        AdapterClass adapter = new AdapterClass(this, images);
        recyclerView.setAdapter(adapter);

        return root;
    }

    void getImages() {
        images.add("https://rukminim1.flixcart.com/image/832/832/book/0/1/9/rich-dad-poor-dad-original-imadat2a4f5vwgzn.jpeg?q=70");
        images.add("https://www.seeken.in/wp-content/uploads/2017/06/The-4-Hour-Work-Week.jpeg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/06/Managing-Oneself.jpeg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/07/How-to-Win-Friends-and-Influence-People.jpeg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/07/THINK-LIKE-DA-VINCI-7-Easy-Steps-to-Boosting-your-Everyday-Genius.jpeg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/07/How-To-Stop-Worrying-And-Start-Living.jpg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/08/THE-INTELLIGENT-INVESTOR.jpeg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/08/Awaken-the-Giant-within-How-to-Take-Immediate-Control-of-Your-Mental-Emotional-Physical-and-Financial-Life.jpg");
        images.add("https://www.seeken.in/wp-content/uploads/2017/08/E-MYTH-REVISITED.jpeg");
        images.add("https://images-na.ssl-images-amazon.com/images/I/41axGE4CehL._SX353_BO1,204,203,200_.jpg");
        images.add("https://rukminim1.flixcart.com/image/832/832/book/0/1/9/rich-dad-poor-dad-original-imadat2a4f5vwgzn.jpeg?q=70");

    }

    @Override
    public void itemClicked(int pos, ModelClass object, View view, String transition) {
        MainActivity activity = (MainActivity) getActivity();
        ChildFragment myFragment = new ChildFragment();
        Bundle bundle = new Bundle();
        bundle.putString("IMAGE_URL", object.getItem(pos));
        bundle.putInt("POSITION", pos);
        bundle.putString("TRANSITION_NAME", transition);
        myFragment.setArguments(bundle);
        Log.i("HOME FRAGMENT-DEBUG", transition + "/" + view.getTransitionName());
        activity.showFragmentWithTransition(this, myFragment, ChildFragment.class.getName(), view, transition);
    }
}